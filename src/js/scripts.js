//@codekit-prepend "hoverIntent.js";
//@codekit-prepend "superfish.js";
//@codekit-prepend "KCT-mobilenav.js";

$("document").ready(function () {
  $(".expand").removeClass("down");
  $(".answer").removeClass("show");

  $(".expand").on("click", function (e) {
    $(this).next(".answer").toggleClass("show");
    $(this).toggleClass("down");
    e.preventDefault();
  });

  $("#menu-main-nav > li > a[href*='#']")
    .css("cursor", "default")
    .on("click", function (e) {
      e.preventDefault();
      return false;
    });

  function scrollToAnswers() {
    $("html, body").animate(
      {
        scrollTop: $("#scrollpoint").offset().top,
      },
      600
    );
  }



  // map code 

  function getQueryParams() {
    let params = new URLSearchParams(window.location.search);
    return {
      county: params.get('county') || '',
    };
  }

  function removeQueryParams() {
    const params = new URLSearchParams(window.location.search);
    params.delete("county");

    // Update the URL without reloading the page
    const newUrl = `${window.location.pathname}`;
    window.history.replaceState({}, '', newUrl);
  }

  const queryParams = getQueryParams();


  function updateQueryParams(county) {
    let params = new URLSearchParams(window.location.search);
    params.set('county', county);

    window
      .history
      .replaceState({}, '', `${window.location.pathname}?${params}`);
  }

  function toggleMSO() {
    var firstItemMso = $("#map-content .active:visible").attr("data-mso");
    $(".mso").removeClass("is-active");

    if (firstItemMso) {
      $('[data-mso-item="' + firstItemMso + '"]').addClass("is-active");
    } else {
      $(".mso:first").addClass("is-active");
    }
  }

  // global filtering function
  function filterByCounty() {
    //reset City select box so it doesn't look false:
    $("#selectCity option:first").prop("selected", true);
    $("#selectSC").val("");

    // clear all active classes on items
    $("#map-content > div").removeClass("active");

    // if a map is selected, sort that first
    var selectedMapRegion =
      $("#map .active").attr("data-name") ||
      $("#map .active").attr("id") ||
      $("[data-virtual-link].active").data("virtual-link") ||
      queryParams.county;

    if (selectedMapRegion) {
      $('[data-county*="' + selectedMapRegion + '"]').addClass("active");
    }

    updateQueryParams(selectedMapRegion);

    toggleMSO();

    let params = new URLSearchParams(window.location.search);
    params.set('county', selectedMapRegion);
  }

  if (queryParams.county) {
    // highlight the county on page load
    $("#map g").attr("class", "");
    if (document.getElementById(queryParams.county)) {
      document.getElementById(queryParams.county).setAttribute("class", "active");
    }
    
    
    // then filter
    filterByCounty();
  }

  $("#selectCity").change(function () {
    $("#map-content > div").removeClass("active");
    $("#map g").attr("class", "");
    $("[data-virtual-link]").removeClass("active");
    $("#selectSC").val("");

    $("#selectCity option:selected").each(function () {
      var cityName = $(this).val();

      if (cityName) {
        $('[data-city*="' + cityName + '"]').addClass("active");
      }
      toggleMSO();
      scrollToAnswers();
    });
    
    removeQueryParams();

  });

  $("#selectSC").change(function () {
    $("#map-content > div").removeClass("active");
    $("[data-virtual-link]").removeClass("active");
    $("#map g").attr("class", "");

    $("#selectSC option:selected").each(function () {
      var scName = $(this).val();
      if (scName) {
        $('[data-sc*="' + scName + '"]').addClass("active");
      }
      toggleMSO();
      scrollToAnswers();
      removeQueryParams();
    });
  });

  // map toggle
  $("#map g").click(function (e) {
    $("[data-virtual-link]").removeClass("active");
    //highlight the active county in the map. note: jquery .addClass doesn't work on SVGs.
    if ($(this).attr("class") == "active") {
      $(this).attr("class", "");
    } else {
      $("#map g").attr("class", "");
      $(this).attr("class", "active");
      scrollToAnswers();
    }

    filterByCounty();
    e.preventDefault();
  });

  // statewide virtual toggle
  $("[data-virtual-link]").click(function (e) {
    $("#map g").attr("class", "");
    //highlight the active county in the map. note: jquery .addClass doesn't work on SVGs.
    if ($(this).attr("class") == "active") {
      $(this).removeClass("active");
    } else {
      $("#map g").attr("class", "");
      $(this).addClass("active");
      filterByCounty();
      scrollToAnswers();
    }

    e.preventDefault();
  });
});

// fire something at the end of a resize event














