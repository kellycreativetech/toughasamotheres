---
layout: base.njk
main_content_image: /site_media/media/flatpage_imgs/get-help.jpg
masthead: /site_media/media/flatpage_imgs/get-help-mast.jpg
permalink: /obtenga-ayuda-ahora/
title: Obtenga Ayuda Ahora
---
<h3 style="margin-bottom:0">Línea de Crisis y Apoyo de Colorado<br/></h3>

<p style="margin-bottom:0">Ayuda disponible para problemas de abuso de sustancias los7 días de la semana, 24 horas al día, 365 días al año. Llame al: 
</p>

<h2 style="margin-top: 0"><a href="tel:+18444938255">1-844-493-TALK (8255)</a><br/><br/></h2>

<h3 style="margin-bottom:0">Línea de Crisis de Mile High United Way</h3>

<div>Encuentre recursos y ayuda en su comunidad. Llame al:</div>

<h2 style="margin-top: 0"><a href="tel:+211">2-1-1</a></h2>