---
layout: base.njk
permalink: /mensaje-para-cada-madre/
title: Mensaje para cada madre
---
## ¡Bienvenida!   

---

Nos alegra que nos visites. Sabemos que no es fácil sentirte vulnerable y buscar asistencia. Pero aquí estás. Celebra tu valentía. Hoy, elijes conocer sobre las posibilidades y oportunidades de una vida donde no tengas que luchar con el consumo de sustancias.

Ponerte a ti misma y tus necesidades primero es el paso más importante, no solo para trabajar hacia la recuperación, sino también para fortalecer a tu familia. Queremos que sepas que no estás sola y que no tienes la culpa de nada. El trastorno por consumo de sustancias (Substance use disorder, SUD) es una enfermedad. Al igual que cualquier otra enfermedad, como la diabetes, la asistencia médica es una parte esencial del tratamiento. Nuestro enfoque es altamente individualizado, ya que a menudo, las mujeres que padecen este trastorno también se enfrentan a traumas y otros problemas de salud mental. 

Buscar asistencia no significa que tengas que ir a tratamiento de inmediato. Trabajaremos juntos para diseñar el nivel de atención que se adapte a tu situación particular y que también satisfaga las necesidades de tu familia.

Sabemos que eres fuerte como una madre, pero no hay necesidad de emprender este viaje sola. Así que muéstranos tu fortaleza y da ese primer paso para comenzar la conversación, nosotros te ayudaremos con el resto. Solo una llamada telefónica o mensaje de texto es el primer paso para recibir asistencia.