---
layout: base.njk
permalink: /acerca/
title: "Acerca de la campa\xF1a"
---

## ¿Qué es Fuerte como una madre?

Fuerte como una madre es una nueva campaña de sensibilización pública que arranca en mayo de 2020 para ayudar a conectar a madres que tienen hijos dependientes con proveedores de tratamiento de consumo de sustancias en comunidades del estado de Colorado. La campaña se basará en una combinación de estrategias, incluida la participación de socios, el alcance comunitario y la publicidad y el marketing en medios tradicionales y digitales con el objetivo de llegar a las madres que luchan con el alcohol u otras drogas.

## ¿Quién patrocina el programa Fuerte como una madre?

Fuerte como una madre es una iniciativa colaborativa lanzada y financiada por Signal Behavioral Health Network, Colorado Office of Behavioral Health y otras organizaciones de servicios administrados en todo el estado, incluidas Diversus Health, Mental Health Partners y West Slope Casa. Depende de socios como tú para ayudar a difundir información a las madres en las comunidades.

<figure style="max-width: 372.522px;"><img height="68" src="https://www.toughasamother.org/dist/img/cbh_logo.svg" width="372.5217391304348"/></figure>

---

<div style="display:flex;justify-content: space-between; align-items: center;margin: 20px 0;flex-wrap: wrap;"> <figure><a href="https://signalbhn.org" target="_blank"  title="opens in a new tab" ><img src="/uploads/signal.png"/></a></figure> <figure><a href="https://diversushealth.org/" target="_blank"  title="opens in a new tab" ><img height="132" src="/dist/img/Diversus_Logo.jpg" width="208" style="-webkit-mix-blend-mode: multiply;mix-blend-mode: multiply; -webkit-filter: grayscale(1); filter: grayscale(1);margin:8px auto;display:block;"/></a></figure> <figure><a href="https://www.mhpcolorado.org" target="_blank"  title="opens in a new tab" ><img src="/uploads/MHP.Logo_bw_300dpi.png" width="253.56643356643357"/></a></figure> <figure><a href="http://www.westslopecasa.com" target="_blank"  title="opens in a new tab" ><img height="115" src="/uploads/WSC.png" width="260.33256880733944"/></a></figure><p></p></div>

---

## ¿Por qué es necesario el programa Fuerte como una madre?

Aunque existen esfuerzos en Colorado y a nivel nacional para atraer a madres embarazadas a tratamientos por consumo de sustancias, existe muy poco alcance directo a madres con hijos dependientes. Debido al estigma social y las percepciones que se tienen de una madre con trastorno por consumo de sustancias, esta población prioritaria ha sido difícil de alcanzar y es muchas veces reacia a buscar tratamiento.
