---
layout: pages/what-to-expect.njk
main_content_image: /uploads/iStock-1131985144.jpg
masthead: ''
permalink: /que-puede-esperar/
title: "Qu\xE9 puede esperar"
---
<ol class="large-numbers"> <li>Obtener asistencia y tratamiento es un signo de fortaleza. Revelar que usas sustancias no significa que perderás a tu hijo o hijos. De hecho, existen programas de tratamiento donde tus hijos pueden estar contigo mientras recibes asistencia.</li><li>Los proveedores de tratamiento que recomendamos no son parte de la corte o de sistemas de bienestar infantil.</li><li>Las mujeres embarazadas y las madres con niños pequeños tienen prioridad. Esto significa que, si hay una lista de espera, recibirán tratamiento primero. <a href="/ayuda-cerca-de-usted">Encuentra asistencia aquí</a>.</li><li>Tu tratamiento será confidencial y sin prejuicios. </li><li>Puedes obtener asistencia y estar en tratamiento con otras mujeres embarazadas o con madres como tú que tienen hijos en casa. </li><li>Los proveedores de tratamiento pueden ser tus defensores. Te darán consejo y te apoyarán durante tu recuperación. Hay fortaleza en ser vulnerable y solicitar ayuda.</li></ol>
