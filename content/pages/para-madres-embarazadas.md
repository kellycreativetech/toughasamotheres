---
layout: pages/full-width-subpage.njk
main_content_image: /uploads/belly.jpg
permalink: /para-madres-embarazadas/
title: Para madres embarazadas
---
El embarazo puede ser un momento emocionante, abrumador y estresante para muchas mujeres, y si usas alcohol o drogas puedes sentirte particularmente agobiada y temerosa de pedir asistencia. Existen riesgos tanto para ti como para el feto, por lo tanto, es especialmente importante buscar asistencia durante este período y no emprender este viaje por tu cuenta. Durante el embarazo, algunos métodos de desintoxicación pueden ser bastante riesgosos para el feto y el tratamiento es crítico tanto para tu salud como para la del bebé. Un médico o proveedor de tratamiento puede darte medicamentos para ayudar a prevenir la abstinencia, de modo que el feto pueda desarrollarse de la forma más saludable posible.
