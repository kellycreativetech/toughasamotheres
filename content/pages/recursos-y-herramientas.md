---
layout: pages/resources.njk
main_content_image: /uploads/moms.jpg
permalink: /recursos-y-herramientas/
sidebar: "Fuerte como una madre colabor\xF3 con el Recovery Cards Project para crear\
  \ una tarjeta de felicitaci\xF3n dise\xF1ada espec\xEDficamente para las madres\
  \ que se enfrentan a enfermedades y trastornos por consumo de sustancias. El Recovery\
  \ Cards Project es una iniciativa de la campa\xF1a Lift The Label y ofrece tarjetas\
  \ de felicitaci\xF3n gratuitas para celebrar la recuperaci\xF3n, brindar \xE1nimo\
  \ a las personas que lo necesitan y expresar gratitud a aquellos que apoyan a otros\
  \ en su proceso de recuperaci\xF3n. Encuentre una tarjeta hoy en [RecoveryCardsProject.com](http://RecoveryCardsProject.com)."
title: Recursos y herramientas
---
Muchas gracias por respaldar el proyecto Fuerte como una madre, la campaña de concientización pública que hemos lanzado en todo el estado para conectar a las madres de Colorado que tienen hijos a su cargo con proveedores de tratamientos para trastornos por consumo de sustancias. Las siguientes herramientas y estrategias lo ayudarán a generar conciencia social en su comunidad.   
  
Valoramos el importante trabajo que realiza cada día para apoyar a las madres de Colorado que padecen trastornos por consumo de sustancias. Gracias a su colaboración, podemos ayudar a garantizar que las mujeres reciban el tratamiento que necesitan para ser madres verdaderamente fuertes.   
  
Si tiene preguntas, necesita asistencia o está buscando un recurso que no se encuentra en este kit de herramientas, o si necesita materiales gráficos listos para ser publicados, comuníquese con Signal Behavioral Health Network a la siguiente dirección de correo electrónico: [outreach@signalbhn.org](mailto:outreach@signalbhn.org).