---
layout: pages/why-help.njk
permalink: /por-qu-pedir-asistencia/
title: "Por qu\xE9 pedir asistencia"
callout_1: |-
  <figure><img alt="Motherhood is hard" src="/dist/img/head-1.svg"></figure>
  <p>Tus hijos te necesitan, pero primero necesitan que estés saludable. La adicción no tiene por qué sacar lo mejor de ti. </p>
callout_1_img: /uploads/iStock-1172768693.jpg
callout_2: |-
  <figure><img alt="Youre not alone" src="/dist/img/head-2.svg"></figure> <p>Ser madre nunca será fácil, pero puede ser mejor. Entre antes pidas ayuda, más rápido podrás obtener asistencia médica para tratar tu mente y cuerpo. </p>
callout_2_img: /uploads/iStock-1168444861.jpg
callout_3: |-
  <figure><img alt="Addiction is a disease" src="/dist/img/head-3.svg"></figure> <p>La adicción es un problema médico, no una carencia ética. Y al igual que otros problemas médicos, el uso de sustancias deteriorará tu salud hasta que recibas tratamiento. </p>
callout_3_img: /uploads/iStock-1134045110.jpg
callout_4: |-
  <figure><img alt="Put yourself first" src="/dist/img/head-4.svg"></figure> <p>Eres más fuerte que tu adicción y pedir asistencia es un signo de tu fortaleza. Eres digna y merecedora de recibir asistencia para mejorarte.</p>
callout_4_img: /uploads/iStock-620382110.jpg
---
Muchas personas no entienden por qué o cómo alguien, especialmente una madre, se vuelve dependiente de las drogas y el alcohol. Pueden suponer que el amor de una madre por sus hijos debería ser suficiente para dejar de usar drogas. No entienden que la adicción es una enfermedad.

En realidad, dejar las drogas requiere mucho más que buenas intenciones o una voluntad fuerte; requiere de la asistencia terapéutica y del tratamiento médico adecuados. Mediante avances científicos, sabemos más que nunca de cómo funcionan las drogas en el cerebro y entendemos mejor cómo tratar el trastorno por consumo de sustancias en las mujeres. Obtén el tratamiento que mereces para que puedas estar saludable, bien y ser la madre más fuerte posible.
