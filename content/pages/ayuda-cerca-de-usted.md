---
layout: pages/map.njk
permalink: /ayuda-cerca-de-usted/
title: Encuentre ayuda cerca de usted
---
Las circunstancias y necesidades de cada madre son diferentes, por lo que recomendamos dar un vistazo al siguiente mapa y luego conversar con alguien que pueda apoyarla en el desarrollo del mejor plan para usted y su familia. Cada región de Colorado tiene una organización de servicios administrados (managed service organization, MSO) que puede ayudar a coordinar la atención con los proveedores. Mientras navega por el mapa, la MSO de esa región se encuentra a continuación. Recomendamos hacer una llamada “sin compromisos” a su MSO para obtener información sobre sus opciones. Ellos conocen a todos los proveedores de su área y pueden ayudarla a conectarse con un plan individualizado y personalizado. Tenga en cuenta que todos los servicios comienzan como servicios para pacientes ambulatorios. Los servicios residenciales se determinan una vez que usted comienza a trabajar con un proveedor que puede determinar sus necesidades y las mejores opciones de tratamiento. **Además, todos nuestros proveedores que se enumeran a continuación aceptan Medicaid**.
