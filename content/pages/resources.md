---
layout: pages/resources.njk
permalink: /resources/
resource1: |-
  <a class="resource ed" href="http://www.colorado.gov/cs/Satellite/CDHS-BehavioralHealth/CBON/1251581543915" target="_blank"  title="opens in a new tab" ><span class="text">Education &amp; Training</span></a>

  <a class="resource clipboard" href="http://www.colorado.gov/cs/Satellite/CDHS-BehavioralHealth/CBON/1251581553844" target="_blank"  title="opens in a new tab" ><span class="text">Evidence Based Policy, Practice &amp; Programs</span></a>

  <a class="resource ident" href="http://www.colorado.gov/cs/Satellite/CDHS-BehavioralHealth/CBON/1251581554094" target="_blank"  title="opens in a new tab" ><span class="text">Licensure &amp; Designation</span></a>
resource2:
  "<a class=\"resource money\" href=\"http://www.colorado.gov/cs/Satellite/CDHS-BehavioralHealth/CBON/1251581553793\"\
  \ target=\"_blank\"><span class=\"text\">Available Funding</span></a>\n\n<a class=\"\
  resource bubble\" href=\"http://www.colorado.gov/cs/Satellite/CDHS-BehavioralHealth/CBON/1251581590304\"\
  \ target=\"_blank\"><span class=\"text\">Women\u2019s Treatment FAQs</span></a>"
resource3:
  "[To find out who to speak to in the Special Connections program, click\
  \ here](http://www.colorado.gov/cs/Satellite/CDHS-BehavioralHealth/CBON/1251581543816).\r\
  \n\nFor guidance on funding women\u2019s gender-responsive treatment in Colorado,\
  \ download \u201CFrom The Horse\u2019s Mouth\u201D, written by Karen Mooney, Colorado\
  \ Office of Behavioral Health Manager of Women\u2019s Substance Use Disorder Programs.\r"
resource4:
  <a class="resource pdf" href="/site_media/media/servee_documents/From_The_Horses_Mouth.pdf"
  target="_blank"  title="opens in a new tab" ><span class="text">Download the PDF</span></a>
title: Resources
sidebar: ""
---

[Click here for an introduction to women’s treatment in Colorado.](http://www.colorado.gov/cs/Satellite?c=Page&childpagename=CDHS-BehavioralHealth%2FCBONLayout&cid=1251581541558&pagename=CBONWrapper)
