---
layout: base.njk
permalink: /informacion/
title: "Obtener Informaci\xF3n sobre el tratamient"
---
Hay una variedad de opciones de tratamiento y asistencia disponibles para las mujeres. Existe una variedad de opciones de tratamiento y asistencia disponibles para ti, incluidas algunas que aceptan a tus hijos. No todos los tratamientos requieren que permanezcas en un centro. Nuestro enfoque de tratamiento tiene en cuenta todas tus necesidades, no solo las médicas. Ofrecemos asistencia para la crianza, así como grupos de asistencia terapéutica entre compañeras.