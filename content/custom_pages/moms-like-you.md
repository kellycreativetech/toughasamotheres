---
layout: pages/videos.njk
permalink: /moms-like-you/
title: Moms Like You
videos:
  - video: https://youtu.be/tnPD566OJv0&amp;rel=0
    photo: /uploads/sydney.jpg
    caption: Sydney went to treatment expecting a long and lonely fight against her addiction. Instead, she found the strength and comfort she always needed.
  - video: https://youtu.be/AkOECjAjIpc&amp;rel=0\
    photo: /uploads/danielle.jpg
    caption: Determination. That’s what it took for Danielle to fight addiction and take back her life.
      Hear more of this mother’s journey to sobriety.
  - video: https://youtu.be/_0o8143qVDQ&amp;rel=0
    photo: /uploads/shelba.jpg
    caption: In order to be the mother she wanted to be, Sheba knew she had to get help. Hear what she’s learned in addiction treatment.

---
