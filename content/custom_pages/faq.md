---
layout: pages/faq.njk
permalink: /preguntas/
title: Preguntas para el centro de tratamiento
faq:
- answer: "Nuestro programa no es punitivo. Es voluntario y seguro.\n\nLos servicios\
    \ sociales solo separan a los ni\xF1os si tienen motivos para creer que existe\
    \ riesgo de abuso o negligencia. Si est\xE1s en tratamiento, est\xE1s demostrando\
    \ quieres mejorar tu vida y la de tus hijos."
  question: "\xBFMe separar\xE1n de mi hijo o hijos si busco tratamiento?"
- answer: "Si a\xFAn no est\xE1s en el sistema, no podemos informar a los servicios\
    \ de protecci\xF3n infantil de tu participaci\xF3n voluntaria en el tratamiento\
    \ o por abandonarlo. Un proveedor de tratamiento solo se comunicar\xE1 con los\
    \ servicios de protecci\xF3n infantil si tus hijos corren el riesgo de abuso o\
    \ descuido, o si tu caso lo maneja un trabajador social y tienes otros ni\xF1\
    os en tu hogar."
  question: "Si decido recibir tratamiento o dejar el tratamiento, \xBFser\xE9 reportada\
    \ a los servicios sociales?  Temo que me abran un caso y estar en \u201Cel sistema\u201D\
    ."
- answer: "Existen recursos de cuidado infantil disponibles a trav\xE9s de ciertos\
    \ proveedores de tratamiento. Algunos ofrecen cuidado infantil en el lugar, otros\
    \ tienen acuerdos con proveedores locales de cuidado infantil y otros te ayudar\xE1\
    n a cubrir el costo.\n\nSi recibes tratamiento hospitalario o residencial, es\
    \ posible que tus hijos puedan acompa\xF1arte. Si est\xE1s en tratamiento ambulatorio,\
    \ puedes traer a tus hijos al programa o al grupo, o puedes disponer de cuidado\
    \ infantil. Aseg\xFArate de preguntarle a tu proveedor de tratamiento sobre las\
    \ opciones de cuidado infantil."
  question: "\xBFPuedo obtener ayuda con el cuido de los ni\xF1os mientras obtengo asistencia\
    \ o mientras estoy en tratamiento?"
- answer: "En Colorado, la mayor\xEDa de los tratamientos \u201Chospitalarios\u201D\
    \ se llaman tratamientos \u201Cresidenciales\u201D, lo que significa que vivir\xE1\
    s en el lugar donde recibes asistencia y tratamiento. Este tipo de atenci\xF3\
    n es mejor para alguien que no podr\xEDa dejar de usar drogas si viviera en su\
    \ casa. El tratamiento \u201Cambulatorio\u201D significa que puedes continuar\
    \ viviendo en tu hogar; tambi\xE9n es m\xE1s com\xFAn que el tratamiento residencial.\
    \ Seg\xFAn el tipo de atenci\xF3n que necesites para dejar de usar drogas, tu\
    \ tratamiento podr\xEDa tomar una o dos horas a la semana o durar varias horas\
    \ al d\xEDa, todos los d\xEDas de la semana."
  question: "\xBFCu\xE1l es la diferencia entre el tratamiento hospitalario y el ambulatorio?"
- answer: "S\xED. Muchos proveedores de tratamiento pueden ayudar con los servicios\
    \ de transporte y el costo. Aseg\xFArate de solicitar m\xE1s informaci\xF3n."
  question: "\xBFPuedo obtener ayuda con el transporte hacia y desde mi lugar de tratamiento?"
- answer: "Todos los pacientes y consejeros en los programas de tratamiento exclusivos\
    \ para mujeres son mujeres. En un centro mixto, estar\xEDas en un grupo exclusivo\
    \ para mujeres la mayor parte del tiempo, pero pueden incorporarse hombres a los\
    \ grupos para algunos temas. En los centros mixtos, tambi\xE9n puedes toparte\
    \ e interactuar con hombres durante las comidas y el per\xEDodo de recreaci\xF3\
    n."
  question: "\xBFEn qu\xE9 se diferencia el tratamiento exclusivo para mujeres del tratamiento\
    \ mixto?"
- answer: "Algunos programas de tratamiento proporcionan pa\xF1ales y f\xF3rmulas\
    \ a los pacientes. En otros casos, el programa WIC, los cupones para alimentos\
    \ (SNAP) o las tarifas del programa pueden cubrir estos art\xEDculos. Obt\xE9\
    n m\xE1s informaci\xF3n sobre el [programa WIC](https://www.coloradowic.gov/)\
    \ y [otros beneficios del estado](https://www.colorado.gov/pacific/cdhs/benefits-assistance)\
    \ de Colorado disponibles para las familias."
  question: "\xBFC\xF3mo pagar\xE9 la f\xF3rmula para beb\xE9, los pa\xF1ales, la comida\
    \ y otras cosas que mis hijos puedan necesitar mientras estoy en tratamiento?"
- answer: "Los programas de tratamiento para mujeres financiados con fondos p\xFA\
    blicos del estado de Colorado aceptan Medicaid. Si no est\xE1s en Medicaid, estos\
    \ programas ofrecen planes de pago flexibles y una escala variable para que te\
    \ asegures de que el costo del tratamiento nunca sea una barrera para ti.\n\n\
    Las mujeres embarazadas que son elegibles para Health First Colorado/Medicaid\
    \ pueden obtener tratamiento gratuito mediante el [programa Special Connections](https://www.colorado.gov/pacific/hcpf/special-connections)."
  question: "\xBFHealth First Colorado/Medicaid cubrir\xE1 mi tratamiento?"
- answer: "S\xED. Tu programa de tratamiento no forma parte de ninguno de los dos\
    \ sistemas, pero tus consejeros y proveedores pueden ayudarte con la defensa y\
    \ la asistencia para ti y tus hijos."
  question: "\xBFMe pueden ayudar si soy parte del bienestar infantil o de sistemas judiciales?"
- answer: "Nunca te expulsar\xE1n del tratamiento si eres sincera acerca de obtener\
    \ asistencia. Las reca\xEDdas y los problemas son se\xF1ales de tu enfermedad\
    \ de adicci\xF3n. El tratamiento consiste en encontrar un camino para superar\
    \ con \xE9xito una adicci\xF3n. Dicho proceso puede tomar varios intentos. Si\
    \ es necesario, tu tratamiento puede cambiar para ayudarte a avanzar. Nuestros\
    \ proveedores de tratamiento est\xE1n aqu\xED para escucharte, brindarte asistencia\
    \ imparcial y guiarte en el camino en el que te encuentres para cambiar y fortalecer\
    \ a tu familia."
  question: "\xBFCu\xE1nto tiempo puedo estar en tratamiento? \xBFMe expulsar\xE1n del\
    \ tratamiento si recaigo o si tengo problemas para progresar?"
- answer: "Es tu decisi\xF3n. Tu familia puede participar en el tratamiento siempre\
    \ que te sientas c\xF3moda con su participaci\xF3n."
  question: "\xBFMi familia puede participar en el tratamiento?"
- answer: "Depende de ti cu\xE1nta informaci\xF3n compartas con tus amigos y familiares.\
    \ El proveedor de tratamiento proteger\xE1 tu privacidad y no compartir\xE1 informaci\xF3\
    n sobre tu tratamiento con nadie sin tu consentimiento."
  question: "\xBFMi tratamiento es confidencial?"
---
¿Ofrecen servicios de guardería durante el tiempo que estaría en tratamiento?
