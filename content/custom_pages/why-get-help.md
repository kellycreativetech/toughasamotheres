---
layout: pages/why-help.njk
permalink: /why-get-help/
title: Why Get Help
callout_1: |-
  <figure><img alt="Motherhood is hard" src="/dist/img/motherhood-is-hard.svg"></figure>
  <p>Your children need you, but they need you to be healthy first. Addiction doesn’t have to get the best of you.<br></p>
callout_1_img: /uploads/iStock-1172768693.jpg
callout_2: |-
  <figure><img alt="Youre not alone" src="/dist/img/youre-not-alone.svg"></figure> <p>Motherhood is never going to be easy, but it can be better. The sooner you ask for support, the quicker you can get medical help that treats your mind and body.</p>
callout_2_img: /uploads/iStock-1168444861.jpg
callout_3: |-
  <figure><img alt="Addiction is a disease" src="/dist/img/addiction-is-a-disease.svg"></figure> <p>It’s ok to admit that you are struggling. Addiction is a medical issue , not a moral shortcoming. Like other medical problems, substance use will harm your health until it gets treated.</p>
callout_3_img: /uploads/iStock-1134045110.jpg
callout_4: |-
  <figure><img alt="Put yourself first" src="/dist/img/put-yourself-first.svg"></figure> <p>Put yourself first. You are stronger than your addiction, and asking for support is a sign of your strength. You are worthy and deserving of thesupport to get better.</p>
callout_4_img: /uploads/iStock-620382110.jpg
---
Many people do not understand why or how someone, especially a mother, becomes dependent on drugs and alcohol. They might assume that a mother’s love for her children should be enough for her to stop using drugs. They don’t get the disease of addiction.

In reality, quitting takes way more than good intentions or a strong will—it takes the right medical treatment and therapeutic support. Through scientific advances, we know more about how drugs work in the brain than ever before, and we better understand how to treat substance use disorder in women. Get the treatment you deserve, so that you can be healthy, well, and the strongest mom possible.
