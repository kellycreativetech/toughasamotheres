---
layout: pages/full-width-subpage.njk
main_content_image: /uploads/belly.jpg
permalink: /for-pregnant-moms/
title: For Pregnant Moms
---
Pregnancy can be an exciting, overwhelming and stressful time for many women, and if you are using alcohol or drugs you may feel particularly overwhelmed and afraid to ask for help. There are risks to both you and the fetus, therefore it is particularly important to seek support during this time and not go on this journey alone. Certain detox methods during pregnancy can be very risky for the fetus and treatment is critical to both you and your baby’s health. A physician or treatment provider can give you medication to help prevent withdrawal so that your fetus can develop as healthy as possible.
