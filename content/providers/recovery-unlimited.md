---
title: Recovery Unlimited
phone: 719-358-7338
site: http://www.recoveryunlimited.biz
tags:
  - Women Only Treatment
  - Teletherapy
county:
  - El Paso
city:
  - Colorado Springs
mso: Diversus Health
---

140 S. Parkside
Colorado Springs, CO 80910
