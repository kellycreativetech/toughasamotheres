---
title: A Turning Point of Colorado Springs, Inc.
phone:
site: http://www.aturningpointcs.org
tags:
  - Pregnant Women Priority Treatment
  - Teletherapy
  - Spanish-speaking services
county:
  - El Paso
city:
  - Colorado Springs
mso: Diversus Health
---

**Union Blvd.**
5160 N. Union Blvd.
<a href="tel:719-550-1010">719-550-1010</a>

**Lake Plaza Dr.**
1221 Lake Plaza Dr.
Suite A
<a href="tel:719-309-6396">719-309-6396</a>
