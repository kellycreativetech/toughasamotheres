---
title: Front Range Clinic
phone: 866-MAT-STAT (866-628-7828)
site: https://www.frontrangemd.com
tags:
  - MAT
  - Individual & Group Counseling
  - Family Support
  - Peer Support
county:
  - Routt
  - Mesa
city:
  - Fort Collins
  - Greeley
  - Lakewood
  - Colorado Springs
  - Pueblo,
  - Steamboat Springs
  - Grand Junction
mso: Rocky Mountain Health Plans
---

Multiple locations- groups mainly virtual

_Mobile MAT available outside of these locations; check website._
