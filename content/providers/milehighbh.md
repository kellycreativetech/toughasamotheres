---
title: Mile High Behavioral Health Integrated Care Summit
phone: 970-485-6676/Adol 303-825-8113 x 150
site: https://www.milehighbehavioralhealthcare.org/integrated-care-summit-county
tags:
  - MAT
  - Oupatient Substance Use Counseling & Mental Health
  - Individual & Group Therapy
  - Peer Recovery Support
  - Case Management
  - LBGTQ2S+ affirming supportive services
  - Adolescent Behavioral Health
county:
  - Summit
city:
  - Frisco
mso: Rocky Mountain Health Plans
---

360 Peak One Drive, #110D
Frisco CO 80443
