---
title: Sobriety House
phone: 720-381-4334
site: http://www.sobrietyhouse.org
tags:
  - Women Only Treatment
  - Pregnant Women Priority Treatment
  - Teletherapy
  - Residential
county:
  - Denver
city:
  - Denver
mso: Signal
---

121 Acoma St.
Denver, CO  80223
