---
title: The Recovery Center
phone: 970-565-4109
site: http://cortezrecovery.org/
county:
  - Montezuma
city:
  - Cortez
mso: Rocky Mountain Health Plans
---

35 N Ash St
Cortez, CO
