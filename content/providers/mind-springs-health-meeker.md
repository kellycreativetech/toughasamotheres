---
title: Mind Springs Health
phone: 970-878-5112
site: https://www.mindspringshealth.org/
tags:
  - Women Only Treatment
  - Teletherapy
county:
  - Rio Blanco
city:
  - Meeker
mso: Rocky Mountain Health Plans
---

267 6th Street
Meeker, CO
