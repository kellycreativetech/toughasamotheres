---
title: Centennial Mental Health Center
phone: 970-848-5412
site: http://www.centennialmhc.org
tags:
  - Childcare
  - Transportation
  - Women Only Treatment
  - Pregnant Women Priority Treatment
  - Teletherapy
  - Spanish-speaking services
county:
  - Yuma
city:
  - Yuma
mso: Signal
---

215 S. Ash Street
Yuma, CO 80759
