---
title: Axis Health System
phone: 970-252-3200
site: http://www.centermh.org
tags:
  - Women Only Treatment
  - Teletherapy
county:
  - Hinsdale
city:
  - Lake City
mso: Rocky Mountain Health Plans
---

700 Henson Street
Lake City, CO
