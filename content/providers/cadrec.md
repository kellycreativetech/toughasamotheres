---
title: Community Alcohol/Drug Rehab & Education Treatment Center (CADREC)
phone: 303-295-2521
site: http://www.facebook.com/cadrecorganization/
tags:
  - Women Only Treatment
  - Pregnant Women Priority Treatment
  - Teletherapy
county:
  - Denver
city:
  - Denver
mso: Signal
---

3315 Gilpin St.
Denver, CO  80205
