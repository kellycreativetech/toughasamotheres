---
title: Paragon Behavioral Health
phone: 303-691-6095
site: https://www.paragonbhc.org
tags:
  - Substance Use & Mental Health treatment
county:
  - Statewide Virtual-only Services
city:
  - Virtual/In home services
mso: Rocky Mountain Health Plans
---

12567 West Cedar Drive Suite 250
Lakewood CO 80228
