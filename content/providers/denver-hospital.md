---
title: Denver Health Hospital and Authority (Outpatient Behavioral Health Services)
phone: 303-436-5699
site: http://www.denverhealth.org/services/behavioral-health/addiction-services/recovery-program-women-families
tags:
  - Childcare
  - Transportation
  - Mother/Child Co-Treatment
  - Women Only Treatment
  - Pregnant Women Priority Treatment
  - Teletherapy
  - Spanish-speaking services
  - Residential
county:
  - Denver
city:
  - Denver
mso: Signal
---

667 Bannock St.
Denver, CO  80204
