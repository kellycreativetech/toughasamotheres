---
title: Mind Springs Health
phone: 970-675-8411
site: https://www.mindspringshealth.org/
tags:
  - Women Only Treatment
  - Teletherapy
county:
  - Rio Blanco
city:
  - Rangely
mso: Rocky Mountain Health Plans
---

17497 West Highway 64
Rangely, CO
