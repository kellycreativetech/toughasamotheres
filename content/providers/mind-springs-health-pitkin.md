---
title: Mind Springs Health
phone: 970-920-5555
site: https://www.mindspringshealth.org/
tags:
  - Women Only Treatment
  - Teletherapy
county:
  - Pitkin
city:
  - Aspen
mso: Rocky Mountain Health Plans
---

0405 Castle Creek Road, Ste 207
Aspen, CO
