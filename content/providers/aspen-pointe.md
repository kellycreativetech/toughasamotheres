---
title: Diversus Health
phone: 719-572-6100
site: https://diversushealth.org/
tags:
  - Pregnant Women Priority Treatment
  - Teletherapy
  - Spanish-speaking services
county:
  - El Paso
city:
  - Colorado Springs
mso: Diversus Health
---

1795 Jetwing Dr.
Colorado Springs, CO 80916
