---
title: Mile High Behavioral Healthcare
phone: 303-825-8113
site: http://milehighbehavioralhealthcare.org
tags:
  - Transportation
  - Women Only Treatment
  - Pregnant Women Priority Treatment
  - Teletherapy
  - Spanish-speaking services
county:
  - Denver
city:
  - Denver
special_connections: 
  - mhbh
mso: Signal
---
4242 Delaware St.
Denver, CO 80040

        
        
