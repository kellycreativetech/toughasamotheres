---
title: Mind Springs Health
phone: 970-879-2141
site: https://www.mindspringshealth.org/
tags:
  - Women Only Treatment
  - Teletherapy
county:
  - Routt
city:
  - Steamboat Springs
mso: Rocky Mountain Health Plans
---

407 South Lincoln Avenue
Steamboat Springs, CO
