---
title: Valley-Wide Health Systems – Southeast Health Group
phone: 800-511-5446
site: https://valley-widehealth.org/
tags:
  - Transportation
  - Mother/Child Co-Treatment
  - Women Only Treatment
  - Pregnant Women Priority Treatment
  - Teletherapy
county:
  - Kiowa
city:
  - Eads
mso: Signal
---

14833 S. Rittgers
Eads, CO 81036
