---
title: St. Mary's Integrated Addiction Medicine
phone: 970-298-3801
site: https://www.sclhealth.org/locations/st-marys-integrated-addiction-medicine/services
tags:
  - Pregnant Women Priority Treatment
  - MAT
  - Adolescent IOP
  - EMDR
  - Individual & Group Therapy
county:
  - Mesa
city:
  - Grand Junction
mso: Rocky Mountain Health Plans
---

2698 Patterson Road- Entrance 43
Grand Junction CO 81506
