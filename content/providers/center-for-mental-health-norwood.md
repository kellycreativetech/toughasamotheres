---
title: Axis Health System
phone: 970-252-3200
site: http://www.centermh.org
tags:
  - Women Only Treatment
  - Teletherapy
county:
  - San Miguel
city:
  - Norwood
mso: Rocky Mountain Health Plans
---
1175 Grand Ave.\
Norwood, CO 81423