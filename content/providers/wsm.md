---
title: West Central Mental Health DBA Solvista Health
phone: 719-275-2351
site: http://solvistahealth.org
tags:
  - Transportation
  - Mother/Child Co-Treatment
  - Pregnant Women Priority Treatment
  - Teletherapy
  - Spanish-speaking services
county:
  - Fremont
  - Chaffee
  - Custer
  - Lake
city:
  - Canon City
  - Salida
  - Westcliffe
  - Leadville
mso: Diversus Health
---

3225 Independence Rd
Canon City, CO 81212
