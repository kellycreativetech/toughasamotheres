---
title: Centennial Mental Health Center
phone: 719-346-8183
site: http://www.centennialmhc.org    
tags:
  - Childcare
  - Transportation
  - Women Only Treatment
  - Pregnant Women Priority Treatment
  - Teletherapy
  - Spanish-speaking services
county:
  - Cheyenne
city:
  - Cheyenne Wells
mso: Signal
---

80 E. 1st Street
Cheyenne Wells, CO  80810
