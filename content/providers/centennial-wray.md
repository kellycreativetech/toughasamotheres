---
title: Centennial Mental Health Center
phone: 970-332-3133
site: http://www.centennialmhc.org    
tags:
  - Childcare
  - Transportation
  - Women Only Treatment
  - Pregnant Women Priority Treatment
  - Teletherapy
  - Spanish-speaking services
county:
  - Yuma
city:
  - Wray
mso: Signal
---

340 Birch Street
Wray, CO 80758
