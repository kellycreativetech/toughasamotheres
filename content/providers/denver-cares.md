---
title: Denver Cares
phone: 303-436-3500
site: https://www.denverhealth.org/services/community-health/denver-cares-detox-drug-alcohol-rehab
tags:
  - Women Only Treatment
  - Pregnant Women Priority Treatment
  - Teletherapy
  - Residential
county:
  - Denver
city:
  - Denver
mso: Signal
---

1155 Cherokee St.
Denver, CO 80204
