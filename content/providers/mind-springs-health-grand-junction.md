---
title: Mind Springs Health
phone: 970-241-6023
site: https://www.mindspringshealth.org/
tags:
  - Women Only Treatment
  - Teletherapy
  - Spanish-speaking services
county:
  - Mesa
city:
  - Grand Junction
special_connections: 
  - mindsprings
mso: Rocky Mountain Health Plans
---

515 28 3/4 Road
Grand Junction, CO



        