---
title: Centennial Mental Health Center
phone: 970-522-4392
site: http://www.centennialmhc.org    
tags:
  - Childcare
  - Transportation
  - Women Only Treatment
  - Pregnant Women Priority Treatment
  - Teletherapy
  - Spanish-speaking services
county:
  - Logan
city:
  - Sterling
mso: Signal
---

211 W. Main Street
Sterling, CO 80751
