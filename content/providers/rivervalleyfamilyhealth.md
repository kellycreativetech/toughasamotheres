---
title: River Valley Family Health Centers
phone: 970-497-3333
site: https://www.rivervalleyfhc.com/emotional-wellness/
tags:
  - MAT
  - Marriage/Family Therapy
  - Trauma
  - Substance Use (Peer Support & Counseling)
county:
  - Montrose
  - Delta
city:
  - Montrose
  - Olathe
  - Delta
mso: Rocky Mountain Health Plans
---

1010 S. Rio Grande
Montrose, CO 81401
