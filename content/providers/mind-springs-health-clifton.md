---
title: Mind Springs Health
phone: 970-245-4213
site: https://www.mindspringshealth.org/
tags:
  - Women Only Treatment
  - Residential
county:
  - Mesa
city:
  - Clifton
mso: Rocky Mountain Health Plans
---

3210 East Road
Clifton, CO
