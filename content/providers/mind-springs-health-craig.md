---
title: Mind Springs Health
phone: 970-824-6541
site: https://www.mindspringshealth.org/
tags:
  - Women Only Treatment
  - Teletherapy
county:
  - Moffat
city:
  - Craig
mso: Rocky Mountain Health Plans
---

439 Breeze St
Craig, CO
