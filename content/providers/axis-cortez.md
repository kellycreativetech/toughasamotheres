---
title: Axis Health System
phone: 970-565-7946
site: http://www.Axishealthsystem.org
tags:
  - Women Only Treatment
  - Teletherapy
  - Spanish-speaking services
county:
  - Montezuma
city:
  - Cortez
mso: Rocky Mountain Health Plans
---

635 E Empire
Cortez, CO
