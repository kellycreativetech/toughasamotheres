---
title: MidValley Family Practice
phone: 970-927-4666
site: http://midvalleyfamilypractice.org/behavioral-health-services/
tags:
  - MAT
  - Adult & Adolescent Individual Therapy
  - All Substances Recovery Group
county:
  - Pitkin
  - Eagle
city:
  - Basalt
mso: Rocky Mountain Health Plans
---

1450 E. Valley Road, Unit 102
Basalt CO 81621
