---
title: North Range Behavioral Health
phone: 970-347-2120
site: http://www.northrange.org
tags:
  - Childcare
  - Transportation
  - Mother/Child Co-Treatment
  - Women Only Treatment
  - Pregnant Women Priority Treatment
  - Teletherapy
  - Spanish-speaking services
  - Residential
county:
  - Weld
city:
  - Greeley
special_connections: 
  - nrbh  
mso: Signal
---
1300 N. 17th Avenue
Greeley, CO 80631



        