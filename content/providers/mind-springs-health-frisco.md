---
title: Mind Springs Health
phone: 970-668-3478
site: https://www.mindspringshealth.org/
tags:
  - Women Only Treatment
  - Teletherapy
county:
  - Summit
city:
  - Frisco
mso: Rocky Mountain Health Plans
---

301 W. Main St, Ste 201
Frisco, CO
