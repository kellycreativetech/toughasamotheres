---
title: Mind Springs Health
phone: 970-723-0055
site: https://www.mindspringshealth.org/
tags:
  - Women Only Treatment
  - Teletherapy
county:
  - Jackson
city:
  - Walden
mso: Rocky Mountain Health Plans
---

Medical Center Building
350 McKinley Street
Walden, CO
