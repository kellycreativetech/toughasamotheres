---
title: Centennial Mental Health Center
phone: 970-474-3769
site: http://www.centennialmhc.org    
tags:
  - Childcare
  - Transportation
  - Women Only Treatment
  - Pregnant Women Priority Treatment
  - Teletherapy
  - Spanish-speaking services
county:
  - Sedgwick
city:
  - Julesburg
mso: Signal
---

118 West Third Street
Julesburg, CO 80737
