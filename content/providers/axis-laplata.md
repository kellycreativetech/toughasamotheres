---
title: Axis Health System
phone: 970-259-2162
site: http://www.Axishealthsystem.org
tags:
  - Women Only Treatment
  - Teletherapy
  - Spanish-speaking services
county:
  - La Plata
city:
  - Durango
mso: Rocky Mountain Health Plans
---


185 Suttle Street
Durango, CO

281 Sawyer Dr, Ste 100
Durango, CO

1125 Three Springs Blvd
Durango, CO
<a href="tel:970-247-5245">970-247-5245</a>
