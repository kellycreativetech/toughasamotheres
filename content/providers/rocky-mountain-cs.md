---
title: Rocky Mountain Behavioral Health
phone: 719-275-7650
site: http://www.rmbh.org
tags:
  - Transportation
  - Mother/Child Co-Treatment
  - Women Only Treatment
  - Pregnant Women Priority Treatment
  - Teletherapy
county:
  - El Paso
city:
  - Colorado Springs
mso: Diversus Health
---

685 Citadel Drive East
Suite 125
