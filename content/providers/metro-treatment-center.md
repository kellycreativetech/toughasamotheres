---
title: Metro Treatment Center (Grand Junction Metro Treatment Center)
phone: 877-284-7074
site: https://www.newseason.com/treatment-center-locations/colorado/grand-junction-metro-treatment-center/
tags:
  - MAT
  - Individual
  - Family
  - Group Substance Use Counseling
county:
  - Mesa
city:
  - Grand Junction
mso: Rocky Mountain Health Plans
---

2956 North Avenue, Unit #1
Grand Junction CO 81501
