---
title: Colorado Treatment Services, LLC
phone: 719-434-2061
site: http://www.crossroadstp.org
tags:
  - Pregnant Women Priority Treatment
  - Teletherapy
county:
  - El Paso
city:
  - Colorado Springs
mso: Diversus Health
---

5360 N Academy Blvd.
Suite 290
Colorado Springs, CO 80918
