---
title: "Bloom - Pikes Peak "
phone: 719-473-5557
site: https://homewardpp.org/what-we-do/treatment/residential-treatment-center/
county:
  - El Paso
city:
  - Colorado Springs
mso: Diversus Health
tags:
  - Transportation
  - Women Only Treatment
  - Pregnant Women Priority Treatment
  - Teletherapy
  - Residential
---

2010 E. Bijou St.
Colorado Springs, CO 80909
