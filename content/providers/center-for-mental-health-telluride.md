---
title: Axis Health System
phone: 970-252-3200
site: http://www.centermh.org
tags:
  - Women Only Treatment
  - Teletherapy
county:
  - San Miguel
city:
  - Telluride
mso: Rocky Mountain Health Plans
---
238 E Colorado Ave., #9\
Telluride, CO 81435