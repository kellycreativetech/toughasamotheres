---
title: Mind Springs Health
phone: 970-887-2179
site: https://www.mindspringshealth.org/
tags:
  - Women Only Treatment
  - Teletherapy
county:
  - Grand_Co
city:
  - Granby
mso: Rocky Mountain Health Plans
---

244 E Agate Ave.
Granby, CO
