---
title: Mind Springs Health
phone: 970-328-6969
site: https://www.mindspringshealth.org/
tags:
  - Women Only Treatment
  - Teletherapy
county:
  - Eagle
city:
  - Eagle
mso: Rocky Mountain Health Plans
---

137 Howard Street
Eagle, CO
