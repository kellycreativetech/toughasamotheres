---
title: Mind Springs Health
phone: 970-476-0930
site: https://www.mindspringshealth.org/
tags:
  - Women Only Treatment
  - Teletherapy
county:
  - Eagle
city:
  - Vail
mso: Rocky Mountain Health Plans
---

395 East Lionshead Cr.
Vail, CO
