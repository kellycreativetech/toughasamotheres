---
title: Mind Springs Health
phone: 970-945-2583
site: https://www.mindspringshealth.org/
tags:
  - Women Only Treatment
  - Teletherapy
county:
  - Garfield
city:
  - Glenwood Springs
mso: Rocky Mountain Health Plans
---

6916 Highway 82
Glenwood Springs, CO
