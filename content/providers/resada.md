---
title: RESADA
site: http://www.resadatreatment.com
tags:
  - Women Only Treatment
  - Pregnant Women Priority Treatment
  - Teletherapy
  - Spanish-speaking services
  - Residential
county:
  - Baca
  - Bent
  - Crowley
  - Kiowa
  - Otero
  - Prowers
  - Las Animas
city:
  - Las Animas
mso: Signal
---

215 East 2nd Street
Las Animas, CO  81054
<a href="tel:719-662-1089">719-662-1089</a>

11000 County Road GG.5
Las Animas, CO  81054
<a href="tel:719-456-2600">719-456-2600</a>
