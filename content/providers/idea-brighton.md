---
title: IDEA Forum, Inc.
phone: 303-659-9440
site: http://www.ideacares.com
tags:
  - Women Only Treatment
  - Pregnant Women Priority Treatment
  - Teletherapy
  - Spanish-speaking services
county:
  - Adams
  - Weld
city:
  - Brighton
mso: Signal
---

**IDEA Brighton**
83 N. 4th Ave.
Brighton, CO 80601
