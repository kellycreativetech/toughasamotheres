---
title: Centennial Mental Health Center
phone: 719-775-2313
site: http://www.centennialmhc.org    
tags:
  - Childcare
  - Transportation
  - Women Only Treatment
  - Pregnant Women Priority Treatment
  - Teletherapy
  - Spanish-speaking services
county:
  - Lincoln
city:
  - Limon
mso: Signal
---

606 Main Street
Limon, CO 80828
