import Post from "/admin/preview-templates/post.js";
import Page from "/admin/preview-templates/page.js";
import Home from "/admin/preview-templates/home.js";
import Product from "/admin/preview-templates/product.js";

// Register the Post component as the preview for entries in the blog collection
CMS.registerPreviewTemplate("messages", Post);
CMS.registerPreviewTemplate("pages", Page);
CMS.registerPreviewTemplate("home", Home);
CMS.registerPreviewTemplate("products", Product);

CMS.registerPreviewStyle("/_includes/assets/css/styles.css");

// Register any CSS file on the home page as a preview style
fetch("/")
  .then(response => response.text())
  .then(html => {
    const f = document.createElement("html");
    f.innerHTML = html;
    Array.from(f.getElementsByTagName("link")).forEach(tag => {
      if (tag.rel == "stylesheet" && !tag.media) {
        CMS.registerPreviewStyle(tag.href);
      }
    });
  });
