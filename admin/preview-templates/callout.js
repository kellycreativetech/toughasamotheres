const CalloutPreview = createClass({
  render() {
    const entry = this.props.entry;
    const title = entry.getIn(["data", "title"]);
    const callout_color = entry.getIn(["data", "callout_color"]);
    const body = entry.getIn(["data", "body"]);

    return html`
    <div className="card product-hat-card ${callout_color}-bg knockout has-squiggles">
      <h2 className="card--eyebrow">${title}</h2>
      ${this.props.widgetFor(body)}
    </div>
    `;
  }
});

CMS.registerPreviewTemplate("callout", CalloutPreview);
