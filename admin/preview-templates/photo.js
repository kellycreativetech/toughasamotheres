const PhotoPreview = createClass({
  render() {
    const entry = this.props.entry;
    const image = entry.getIn(["data", "image"]);
    const caption = entry.getIn(["data", "caption"]);

    return html`
    <figure className="sidebar-photo">
      <img src="${image}" />
      <figcaption>${this.props.widgetFor(caption)}</figcaption>
    </figure>
    `;
  }
});

CMS.registerPreviewTemplate("photo", PhotoPreview);
