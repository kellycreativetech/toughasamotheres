import htm from "https://unpkg.com/htm?module";

const html = htm.bind(h);

// Preview component for a Page
const Home = createClass({
  render() {
    const entry = this.props.entry;
    const masthead = entry.getIn(["data", "masthead"]);
    const callouts = entry.getIn(["data", "callouts"]);

    return html`
      <section className="masthead">
        <div className="glide">
          <div className="glide__track" data-glide-el="track">
            <ul className="glide__slides">
              ${masthead ?
                this.props.widgetsFor('masthead').map(
                  img =>
                    html`
                      <li className="glide__slide">
                        <div className="masthead--slide">
                          <img className="masthead--img" src="${img.getIn(['data', 'image'])}" />
                          <h1 className="masthead--home-heading">${img.getIn(['data', 'title'])}</h1>
                        </div>
                      </li>
                    `)
                : null}
            </ul>
          </div>
        </div>
      </section>

      <section className="l-container skinny this-section-intrudes">
        <div className="card text-center large-text">

          <h1><br /><br /><br />${entry.getIn(["data", "title"], null)}</h1>
          <div className="logo-row">
            <img src="/dist/img/true-gum.svg" alt="True Gum logo" />
            <img src="/dist/img/sim-plete.svg" alt="Sim-plete logo" />
            <img src="/dist/img/colony-organics.svg" alt="Colony Organics logo" />
          </div>
          ${this.props.widgetFor("body")}
        </div>
      </section>

      <section className="l-container skinny m-top" >
        <div className="card blue-bg knockout text-center has-squiggles next-section-intrudes">
          <p className="card--eyebrow">Explore</p>
          <h2 className="h1">${entry.getIn(["data", "section_1_head"], null)}</h2>
        </div>
      </section>

      <section className="l-container this-section-intrudes">
        <div className="thirds-grid">
        ${callouts ?
          this.props.widgetsFor('callouts').map(
            img =>
              html`
               <div
                  className="card logo-card"
                >
                  <img src="${img.getIn(['data', 'logo'])}" />
                  <div className="logo-card--details">

                  ${img.getIn(['widgets', 'text'])}
                  </div>
                </div>
              `)
          : null}
        </div>
      </section>

      <section className="l-container skinny m-top" >
        <div className="card green-bg knockout text-center large-text has-squiggles next-section-intrudes">
          <p className="card--eyebrow">Mission</p>
          <h2 className="h1">${entry.getIn(["data", "section_2_head"], null)}</h2>
          ${this.props.widgetFor("section_2")}
        </div>
      </section>
    `;
  }
});

export default Home;
