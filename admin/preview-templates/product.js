import htm from "https://unpkg.com/htm?module";
import format from "https://unpkg.com/date-fns@2.0.0-alpha.2/esm/format/index.js?module";

const html = htm.bind(h);

// Preview component for a Post
const Product = createClass({
  render() {
    const entry = this.props.entry;
    const metaTitle = entry.getIn(["data", "pagemeta", "title"]);
    const metaDesc = entry.getIn(["data", "pagemeta", "description"]);
    const metaImg = entry.getIn(["data", "pagemeta", "meta_image"]);
    const masthead = '/uploads/' + entry.getIn(["data", "masthead"]);
    const image = '/uploads/' + entry.getIn(["data", "image"]);
    const doc = entry.getIn(["data", "document"]);

    return html`
    ${console.log('image attempt', this.props.getAsset(masthead))}
    <section className="masthead--product">
      <img className="masthead--img"
        src="${masthead ? masthead : '/dist/img/agar.jpg'}"
      />

      <div className="l-container">
        <h1 className="masthead--product-heading">${entry.getIn(["data", "title"], null)}</h1>
      </div>
    </section>

      <div className="card">
          <div className="thirds-grid this-section-intrudes text-center">
            <div className="card product-hat-card blue-bg knockout has-squiggles">
              <h2 className="card--eyebrow">Common Names</h2>
              ${this.props.widgetFor("common_names")}
            </div>
            <div className="card product-hat-card blue-bg knockout has-squiggles">
              <h2 className="card--eyebrow">Functionality</h2>
              ${this.props.widgetFor("functionality")}
            </div>
            <div className="card product-hat-card blue-bg knockout has-squiggles">
              <h2 className="card--eyebrow">Quality</h2>
              ${this.props.widgetFor("quality")}
            </div>
          </div>

          <h2 className="knockout-heading">Applications for ${entry.getIn(["data", "title"], null)}</h2>

          <div className="two-thirds-grid">
            <div className="sidebar has-callout-lists">
              ${this.props.widgetFor("applications")}

              ${doc? html`
                <a href="${entry.getIn(["data", "document"], null)}" className="button file-dl" download>
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 450.2 512">
                    <path d="M393.299,404.286l-21.213-21.213-15.914,15.914V354.285h-30v44.702l-15.914-15.914-21.213,21.213,52.1269,52.126Z" fill="currentColor"/>
                    <path d="M388.399,293.945V0H0V512H450.2V293.945ZM30,482V30H358.398V293.945H232.145V482Zm390.2,0H262.146V323.945H420.2Z" fill="currentColor"/>
                    <path d="M75,74.602H223.917v30H75Z" fill="currentColor"/>
                    <path d="M75,194.602H259.193v30H75Z" fill="currentColor"/>
                    <path d="M75,314.602h92.097v30H75Z" fill="currentColor"/>
                    <path d="M75,254.602h92.097v30H75Z" fill="currentColor"/>
                    <path d="M75,134.602H313.398v30H75Z" fill="currentColor"/>
                  </svg>
                  Download PDF
                </a>` : null}
            </div>
            <div className="main-content">${this.props.widgetFor("body")}</div>
          </div>

          <div className="thirds-grid">
            <div >
              <h2 className="knockout-heading">Botanical Sources</h2>
              ${this.props.widgetFor("botanical_sources")}
            </div>

            <div>
            <img src="${image ? image : '/dist/img/agar-2.jpg'}" />
            </div>

            <div>
              <h2 className="knockout-heading">Polymer Chemistry</h2>
              ${this.props.widgetFor("polymer_chemistry")}
            </div>
          </div>
      </div>

    `;
  }
});

export default Product;
