import htm from "https://unpkg.com/htm?module";
import format from "https://unpkg.com/date-fns@2.0.0-alpha.2/esm/format/index.js?module";

const html = htm.bind(h);

// Preview component for a Post
const Post = createClass({
  render() {
    const entry = this.props.entry;
    const metaTitle = entry.getIn(["data", "pagemeta", "title"]);
    const metaDesc = entry.getIn(["data", "pagemeta", "description"]);
    const metaImg = entry.getIn(["data", "pagemeta", "meta_image"]);
    const videoFeed = entry.getIn(["data", "video_feed"]);
    const audio = entry.getIn(["data", "audio"]);
    const author = entry.getIn(["data", "author"]);

    return html`
      <main>
        <article>
          <h1>${entry.getIn(["data", "title"], null)}</h1>
          <p>
            <small>
              <time
                >${
                  format(
                    entry.getIn(["data", "date"], new Date()),
                    "DD MMM, YYYY"
                  )
                }</time
              >
              ${author && html` by ${entry.getIn(["data", "author"], '')}`}
            </small>
          </p>

          ${audio && html`<audio controls><source src="${entry.getIn(["data", "audio"], null)}"/></audio>`}
          ${videoFeed && html`<h5><em>Live video feed will be enabled on the live site.</em></h5>`}




          <p>${entry.getIn(["data", "summary"], "")}</p>

          ${this.props.widgetFor("body")}
          <p>
            ${
              entry.getIn(["data", "tags"], []).map(
                tag =>
                  html`
                    <a href="#" rel="tag">${tag}</a>
                  `
              )
            }
          </p>
        </article>
      </main>
    `;
  }
});

export default Post;
