import htm from "https://unpkg.com/htm?module";

const html = htm.bind(h);

// Preview component for a Page
const Page = createClass({
  render() {
    const entry = this.props.entry;
    const certificates = entry.getIn(["data", "certificates"]);
    const masthead = '/uploads/' + entry.getIn(["data", "masthead"]);
    const videoFeed = entry.getIn(["data", "video_feed"]);
    const metaTitle = entry.getIn(["data", "pagemeta", "title"]);
    const metaDesc = entry.getIn(["data", "pagemeta", "description"]);
    const metaImg = entry.getIn(["data", "pagemeta", "meta_image"]);
    const mastText = entry.getIn(["data", "masthead", "mastheadText"]);
    const sidebar = entry.getIn(["data", "sidebar"]);

    return html`

      <section className="masthead--sub">
        <img className="masthead--img"
          src="${masthead ? masthead : '/dist/img/agar.jpg'}" />
        <div className="l-container">
          <h1 className="masthead--sub-heading">${entry.getIn(["data", "title"], null)}</h1>
        </div>
      </section>

      <main className="site-main l-container">
      <div className="card">
        ${this.props.widgetFor("body")}


        ${sidebar ?
          this.props.widgetsFor('sidebar').map(
            item =>
              html`
              <!-- coming soon -->
              `)
          : null}



        <ul className="cert-docs">
        ${certificates ?
          this.props.widgetsFor('certificates').map(
            img =>
              html`
                <li>
                <img src="${'http://colonygums.imgix.net/uploads/' + img.getIn(['data', 'file']) + '?w=300&h=300'}" />
                <p>${img.getIn(['data', 'title'])}</p>
                </li>
              `)
          : null}
        </ul>
      </div>
      </main>
    `;
  }
});

export default Page;
