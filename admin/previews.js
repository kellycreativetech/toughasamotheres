const {
  w3DateFilter,
  markdownFilter,
  dateFilter,
  helpers,
  slugify,
} = previewUtil;

const env = nunjucks.configure();

env.addFilter('markdownFilter', markdownFilter);
env.addFilter('md', markdownFilter);
env.addFilter('slug', slugify);
env.addFilter('dateFilter', dateFilter);

// fake imgix filter to use local paths
env.addFilter("imgix", function(img) {
  let imgURL = img;
  return imgURL;
});


const Preview = ({ entry, path, context }) => {
  const data = context(entry.get('data').toJS());
  const html = env.render(path, { ...data, helpers });
  return <div dangerouslySetInnerHTML={{ __html: html }}/>
};


const Home = ({ entry }) => (
  <Preview
    entry={entry}
    path="pages/home.njk"
    context={({ title, body, homecallout2, homecallout3, mastheadtext }) => ({
      title,
      layoutContent: markdownFilter(body),
      homecallout2,
      homecallout3,
      mastheadtext,
    })}
  />
);

const Provider = ({ entry }) => (
  <Preview
    entry={entry}
    path="pages/provider.njk"
    context={({ title, phone, site, tags, county, city, mso, body }) => ({
      title,
      phone,
      site,
      tags,
      county,
      city,
      mso,
      layoutContent: markdownFilter(body || ''),
    })}
  />
);

const Faq = ({ entry }) => (
  <Preview
    entry={entry}
    path="pages/faq.njk"
    context={({ title, body, faq, question, answer }) => ({
      title,
      faq,
      question,
      answer,
      layoutContent: markdownFilter(body || ''),
    })}
  />
);

const WhyGetHelp = ({ entry }) => (
  <Preview
    entry={entry}
    path="pages/why-help.njk"
    context={({ title, body, callout_1, callout_1_img, callout_2, callout_2_img, callout_3, callout_3_img, callout_4, callout_4_img }) => ({
      title,
      callout_1,
      callout_1_img,
      callout_2,
      callout_2_img,
      callout_3,
      callout_3_img,
      callout_4,
      callout_4_img,
      layoutContent: markdownFilter(body || ''),
    })}
  />
);

const ForPregnantMoms = ({ entry }) => (
  <Preview
    entry={entry}
    path="pages/full-width-subpage.njk"
    context={({ title, body, main_content_image }) => ({
      title,
      main_content_image,
      layoutContent: markdownFilter(body || ''),
    })}
  />
);

const Page = ({ entry }) => (
  <Preview
    entry={entry}
    path="base.njk"
    context={({ title, body }) => ({
      title,
      layoutContent: markdownFilter(body || ''),
    })}
  />
);

CMS.registerPreviewTemplate('home', Home);
CMS.registerPreviewTemplate('provider', Provider);
CMS.registerPreviewTemplate('pages', Page);
CMS.registerPreviewTemplate('faq', Faq);
CMS.registerPreviewTemplate('whyhelp', WhyGetHelp);
CMS.registerPreviewTemplate('forpregnantmoms', ForPregnantMoms);

CMS.registerPreviewStyle(`/_includes/assets/css/styles.css`, { raw: true })

// Register any CSS file on the home page as a preview style
fetch("/")
  .then(response => response.text())
  .then(html => {
    const f = document.createElement("html");
    f.innerHTML = html;
    Array.from(f.getElementsByTagName("link")).forEach(tag => {
      if (tag.rel == "stylesheet" && !tag.media) {
        CMS.registerPreviewStyle(tag.href);
      }
    });
  });
