const { DateTime } = require("luxon");
const CleanCSS = require("clean-css");
const UglifyJS = require("uglify-es");
const htmlmin = require("html-minifier");
const pluginRss = require("@11ty/eleventy-plugin-rss");
const dateFilter = require('./src/js/filters/date-filter.js');
const markdownFilter = require('./src/js/filters/markdown-filter.js');
const w3DateFilter = require('./src/js/filters/w3-date-filter.js');

module.exports = function(eleventyConfig) {
  eleventyConfig.addLayoutAlias("post", "post.njk");

  // RSS plugin
  eleventyConfig.addPlugin(pluginRss);

  eleventyConfig.addFilter('dateFilter', dateFilter);
  eleventyConfig.addFilter('markdownFilter', markdownFilter);
  eleventyConfig.addFilter('w3DateFilter', w3DateFilter);

  // Date formatting (human readable)
  eleventyConfig.addFilter("readableDate", dateObj => {
    return DateTime.fromJSDate(dateObj).toFormat("LLL dd, yyyy");
  });

  // Date formatting (year, for footer)
  eleventyConfig.addFilter("year", dateObj => {
    return DateTime.local().toFormat("yyyy");
  });

  // Date formatting (machine readable)
  eleventyConfig.addFilter("machineDate", dateObj => {
    return DateTime.fromJSDate(dateObj).toFormat("yyyy-MM-dd");
  });

  // for cachebusting
  eleventyConfig.addFilter("cacheBustDate", dateObj => {
    return DateTime.local().toFormat("yyyy-MM-ddThh:mm:ss");
  });

  // Minify CSS
  eleventyConfig.addFilter("cssmin", function(code) {
    return new CleanCSS({}).minify(code).styles;
  });

  // Minify JS
  eleventyConfig.addFilter("jsmin", function(code) {
    let minified = UglifyJS.minify(code);
    if (minified.error) {
      console.log("UglifyJS error: ", minified.error);
      return code;
    }
    return minified.code;
  });

  // Imgix Filter
  eleventyConfig.addFilter("imgix", function(img) {
    let imgURL = 'https://toughasamother.imgix.net/' + img;
    return imgURL;
  });

  // Minify HTML output
  eleventyConfig.addTransform("htmlmin", function(content, outputPath) {
    if (outputPath.indexOf(".html") > -1) {
      let minified = htmlmin.minify(content, {
        useShortDoctype: true,
        removeComments: true,
        collapseWhitespace: true
      });
      return minified;
    }
    return content;
  });

  // only content in the `products/` directory
  eleventyConfig.addCollection("providers" , function(collection) {
    return collection.getAllSorted().filter(function(item) {
      return item.inputPath.match(/^\.\/content\/providers\//) !== null;
    });
  });

  // Don't process folders with static assets e.g. images
  eleventyConfig.addPassthroughCopy({ "src/img": "dist/img" });
  eleventyConfig.addPassthroughCopy({"src/uploads": "uploads"});
  eleventyConfig.addPassthroughCopy({"src/resources": "resources"});
  eleventyConfig.addPassthroughCopy({"src/js": "dist/js"});
  eleventyConfig.addPassthroughCopy({"src/js/min": "dist/js/min"});
  eleventyConfig.addPassthroughCopy("_includes/assets");
  eleventyConfig.addPassthroughCopy("admin");
  eleventyConfig.addPassthroughCopy({"node_modules/nunjucks/browser/nunjucks-slim.js": "dist/js/nunjucks-slim.js"});

  eleventyConfig.setUseGitIgnore(false);

  /* Markdown Plugins */
  let markdownIt = require("markdown-it");
  let markdownItAnchor = require("markdown-it-anchor");
  let options = {
    html: true,
    breaks: true,
    linkify: true
  };
  let opts = {
    permalink: false
  };

  eleventyConfig.setLibrary("md", markdownIt(options)
    .use(markdownItAnchor, opts)
  );


  // specifically for the template filter
  const md = require('markdown-it')({
      html: true,
      breaks: true,
      linkify: true
  });

  eleventyConfig.addNunjucksFilter("md", markdownString => md.render(markdownString));

  return {
    templateFormats: ["md", "njk", "html", "liquid"],

    // If your site lives in a different subdirectory, change this.
    // Leading or trailing slashes are all normalized away, so don’t worry about it.
    // If you don’t have a subdirectory, use "" or "/" (they do the same thing)
    // This is only used for URLs (it does not affect your file structure)
    pathPrefix: "/",

    markdownTemplateEngine: "liquid",
    htmlTemplateEngine: "njk",
    dataTemplateEngine: "njk",
    passthroughFileCopy: true,
    dir: {
      input: ".",
      includes: "_includes",
      data: "_data",
      output: "_site"
    }
  };
};
